package ru.t1.godyna.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.godyna.tm.api.service.IProjectTaskService;
import ru.t1.godyna.tm.api.service.ITaskService;
import ru.t1.godyna.tm.command.AbstractCommand;
import ru.t1.godyna.tm.enumerated.Role;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    protected ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    @NotNull
    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
