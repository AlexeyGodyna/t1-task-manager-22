package ru.t1.godyna.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private final String NAME = "about";

    @NotNull
    private final String ARGUMENT = "-a";

    @NotNull
    private final String DESCRIPTION = "Shows developer info";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Alexey Godyna");
        System.out.println("E-mail: agodyna@t1-consulting.ru");
        System.out.println("E-mail: algo2791@yandex.ru");
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
