package ru.t1.godyna.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

public abstract class AbstractModel {

    @Getter
    @Setter
    @NotNull
    private String id = UUID.randomUUID().toString();

}
