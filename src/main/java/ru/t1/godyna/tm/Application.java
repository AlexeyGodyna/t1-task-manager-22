package ru.t1.godyna.tm;

import org.jetbrains.annotations.NotNull;
import ru.t1.godyna.tm.component.Bootstrap;

public final class Application {

    public static void main(final String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}
