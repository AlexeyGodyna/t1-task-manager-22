package ru.t1.godyna.tm.exception.field;

public class UserIdEmptyException extends AbsractFieldException {

    public UserIdEmptyException() {
        super("Error! User Id not found...");
    }

}
